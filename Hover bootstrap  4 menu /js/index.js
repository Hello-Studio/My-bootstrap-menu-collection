// Dropdown navbar menu on hover
$('.dropdown-menu', this).css('margin-top', 0);
$('.dropdown').hover(function () {
    $('.dropdown-toggle', this).trigger('click').toggleClass("disabled");
});